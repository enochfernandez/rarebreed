package com.example.connectivity

import okhttp3.ResponseBody

/**
 * Wrapper for data provided from the response[DogBreedsResponse] classes fetched
 * from the service[RareBreedService] classes
 */
sealed class Resource<out T> {
    data class Success<out T>(val value: T) : Resource<T>()
    data class Failure(
        val isNetworkError: Boolean,
        val errorCode: Int?,
        val errorBody: ResponseBody?,
        val errorMessage: String = ""
    ) : Resource<Nothing>()

    data object Loading : Resource<Nothing>()

    inline fun onSuccess(block: (T) -> Unit): Resource<T> {
        return apply {
            if (this is Success) {
                block(value)
            }
        }
    }

    inline fun onFailure(block: (errorMessage : String, isNetworkError: Boolean) -> Unit): Resource<T> {
        return apply {
            if (this is Failure) {
                block(errorMessage, isNetworkError)
            }
        }
    }
}