package com.example.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Network representation of getting all breeds when fetched from breeds/list/all
 */
@JsonClass(generateAdapter = true)
data class DogBreedsResponse(
    @Json(name = "message")
    val breeds: Map<String, List<String>>,
    val status: String
)

/**
 * Network representation of getting subBreeds when fetched from breed/{breed}/list
 */
@JsonClass(generateAdapter = true)
data class DogSubBreedsResponse(
    @Json(name = "message")
    val subBreeds: List<String>,
    val status: String
)