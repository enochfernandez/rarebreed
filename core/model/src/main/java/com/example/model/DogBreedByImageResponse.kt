package com.example.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

/**
 * Network representation of random breed image when fetched from breed/{breed}/images/random
 */
@JsonClass(generateAdapter = true)
data class DogBreedByImageResponse (
    @Json(name = "message")
    val imageUrl: String,
    val status: String
)

/**
 * Network representation of random breed images when fetched from breed/{breed}/images/random/{images}
 */
@JsonClass(generateAdapter = true)
data class DogBreedRandomImagesResponse(
    @Json(name = "message")
    val breedImageUrls: List<String>,
    val status: String
)