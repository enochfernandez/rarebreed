package com.example.myapplication

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.connectivity.NetworkMonitor
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class MainActivityViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: MainActivityViewModel
    private lateinit var networkMonitor: NetworkMonitor
    private val isOnlineFlow = MutableStateFlow(true)

    @Before
    fun setup() {
        networkMonitor = mockk()

        coEvery { networkMonitor.isOnline } returns isOnlineFlow
        viewModel = MainActivityViewModel(networkMonitor)
    }


    @Test
    fun `test isOffline when online`() = runTest {
        // Mock NetworkMonitor
        //Dispatchers.setMain(UnconfinedTestDispatcher())

        // Collect the value emitted by isOffline
        val isOfflineValues = mutableListOf<Boolean>()
        //val isOnlineFlow = MutableStateFlow(true)
        //every { networkMonitor.isOnline } returns isOnlineFlow


        //networkMonitor.setConnected(false)
        val job = launch {
            viewModel.isOffline.collect {
                isOfflineValues.add(it)
            }
        }

        // Ensure the initial value is true (offline)
        val initialValue = isOfflineValues.first()
        //assert(initialValue)

        isOnlineFlow.value = false
        // Trigger a change in the NetworkMonitor to indicate offline state


        // Wait for the value to be collected
        advanceUntilIdle()

        // Verify that isOffline is still true (offline)
        val latestValue = isOfflineValues.last()
        assert(latestValue)

        // Cancel the job
        job.cancel()
    }
}