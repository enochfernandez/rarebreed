package com.example.myapplication.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.breeddetail.BreedDetailScreen
import com.example.breeddetail.BreedDetailViewModel
import com.example.breedhome.BreedHomeScreen
import com.example.myapplication.ui.Screen
import com.example.myapplication.ui.utils.assistedViewModel

const val RARE_BREED_HOST_ROUTE = "rare-breed-main-route"
@Composable
fun RareBreedNavigation() {
    val navController = rememberNavController()

    NavHost(navController, startDestination = Screen.Breed.route, route = RARE_BREED_HOST_ROUTE) {
        composable(Screen.Breed.route) {
            BreedHomeScreen {
                navController.navigate(Screen.BreedDetail.route(it))
            }
        }

        composable(
            Screen.BreedDetail.route,
            arguments =  listOf(
                navArgument(Screen.BreedDetail.ARG_BREED_ID) { type = NavType.StringType }
            )
        ) {
            val breedId = requireNotNull(it.arguments?.getString(Screen.BreedDetail.ARG_BREED_ID))
            BreedDetailScreen(
                viewModel = assistedViewModel {
                    BreedDetailViewModel.provideFactory(breedDetailViewModelFactory(), breedId)
                },
                onNavigateUp = { navController.navigateUp() }
            )
        }
    }
}