package com.example.myapplication.ui

/**
 * Navigation class for defining all screens
 */
sealed class Screen(val route: String, val name: String) {

    data object Breed : Screen("breed", "Breed")

    data object BreedDetail : Screen("breed/{breedId}", "Breed Details") {
        fun route(breedId: String): String {
            return "breed/$breedId"
        }

        const val ARG_BREED_ID: String = "breedId"
    }
}