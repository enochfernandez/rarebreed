
RareBreed App
==================

RareBreed App is a 2 screen Android app built with Kotlin and Jetpack Compose (main focus of the code development) and also 
XML layout. It follows a multi feature module architecture that is useful for scalability of code and parallelization.  
When App is run it provides users with a list of dog breeds, clicking on any of this list shows sub breeds and its
images associated with this breed

This is the repository for the [RareBreed](https://gitlab.com/enochfernandez/rarebreed.git) app.

# Features

The codebase focuses on following key things:

- [x] Single Activity Design
- [x] Jetpack Compose UI
- [x] Clean and Simple Material UI
- [x] XML Layout
- [x] Modularization
- [x] Tests

# Architecture
The uses a Single Activity and Navigation approach. Below are the modules contained in the app
- [**`:composeapp`**] : This contains implementation to build the UI Compose version for the app , this module depends 
- can depend on other feature modules and core modules
- [**`:network`**] : This contains code for network connection to fetch data, this can depend on some core modules but 
- not feature modules
- [**`:data"`**] : Contains implementation of code that manages data fetched from data sources e.g contains the repository
- classes. This can depend on the network module and some core modules but not feature modules
- [**`:core:connectivity`**] - Contain common code for anything related to connectivity of the app
- [**`:core:model`**] -  Contain common code for model classes. Core modules usually dont depend on other modules
- [**`:features:breeddetail`**] Contains code related to the Breed Details page which would show images of a selected
- breed. The list of sub breeds associated with breed is also displayed. This depends on the data module and other core
- modules
- [**`:features:breedhome`**] Contains code related to the Breed page which would show a list of breeds
- This depends on the data module and other core modules but dont depend on other feature modules
- [**`:simpleapp`**] - Implementation of code to build the XML Layout Fragment app

This app uses [_**MVVM (Model View View-Model)**_](https://developer.android.com/jetpack/docs/guide#recommended-app-arch) architecture.