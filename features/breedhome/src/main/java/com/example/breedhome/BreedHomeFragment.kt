package com.example.breedhome

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import com.example.breedhome.databinding.FragmentBreedHomeListBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

/**
 * A fragment representing a list of Items for displaying Breeds
 */
@AndroidEntryPoint
class BreedHomeFragment : Fragment() {

    private val breedHomeViewModel: BreedHomeViewModel by viewModels()

    private var _binding: FragmentBreedHomeListBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentBreedHomeListBinding.inflate(inflater, container, false)
        var view = binding.root
        view.layoutManager = LinearLayoutManager(context)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                breedHomeViewModel.homeBreedState.collect {
                    val view = binding.root
                    view.adapter = MyBreedRecyclerViewAdapter(it.breeds, ::onBreedClicked)
                }
            }
        }
    }


    private fun onBreedClicked(breed: String) {
        val request = NavDeepLinkRequest.Builder
            .fromUri("android-app://example.rare_breed/action_breedFragment_to_breedDetailFragment/$breed".toUri())
            .build()
        findNavController().navigate(request)
    }
}