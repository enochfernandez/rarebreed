package com.example.breedhome

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.ui.FailureDialog
import com.example.ui.LoaderDialog

@Composable
fun BreedHomeScreen(
    viewModel: BreedHomeViewModel = hiltViewModel(),
    onNavigateToBreedDetail: (String) -> Unit
) {

    val state by viewModel.homeBreedState.collectAsStateWithLifecycle()

    BreedHomeContent(
        isLoading = state.isLoading,
        breedData = state.breeds,
        error = state.errorMessage,
        isNetworkError = state.isNetworkError,
        onNavigateToBreedDetail = onNavigateToBreedDetail
    )

}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BreedHomeContent(
    isLoading: Boolean,
    breedData: List<String>,
    error: String?,
    isNetworkError: Boolean,
    onNavigateToBreedDetail: (String) -> Unit
) {

    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Row {
                        val image = painterResource(id = R.drawable.dog)
                        Image(painter = image, contentDescription = "Breed Icon")
                        Spacer(modifier = Modifier.width(12.dp))
                        Text(
                            text = "RareBreed",
                            textAlign = TextAlign.Start,
                            color = MaterialTheme.colorScheme.scrim,
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.outline,
                    titleContentColor = MaterialTheme.colorScheme.scrim
                ),

            )
        }
    ) { padding ->

        if (isLoading) {
            LoaderDialog()
        }

        if (error != null || isNetworkError) {
            FailureDialog(error ?: "Error Occurred, Try Again")
        }

        val modifier = Modifier.padding(padding)
        BreedList(breedData.toList(), modifier) { breed ->
            onNavigateToBreedDetail(breed)
        }
    }


}

@Composable
fun BreedList(breeds: List<String>, modifier: Modifier, onClick: (String) -> Unit) {
    LazyColumn (
        contentPadding = PaddingValues(vertical = 4.dp),
        modifier = modifier.testTag("breedList")
    ) {
        items(
            items = breeds,
            itemContent = { breed ->
                BreedCard(breed) { onClick(breed) }
            }
        )
    }
}

@Composable
fun BreedCard(name: String, onBreedClick: () -> Unit) {
    Card(
        shape = RoundedCornerShape(4.dp),
        modifier = Modifier
            .padding(horizontal = 8.dp)
            .fillMaxWidth()
            .height(70.dp)
            .background(color = MaterialTheme.colorScheme.surface)
            .wrapContentHeight()
            .clickable { onBreedClick() },
        elevation = CardDefaults.cardElevation(0.dp)
    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
        ) {
            Text(
                text = name,
                color = MaterialTheme.colorScheme.scrim,
                style = typography.bodyLarge,
                textAlign = TextAlign.Center,
                fontWeight = FontWeight.Bold,
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}

@Preview(showBackground = true, backgroundColor = 0xFFF0EAE2)
@Composable
fun PreviewAreaViewPager(modifier: Modifier = Modifier) {
    BreedHomeContent(
        false,
        mutableListOf("One","Two","Three","Four","Five","Six"),
        null,
    false,
        {  }
    )
}
