package com.example.breedhome

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.connectivity.Resource
import com.example.data.RareBreedRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import javax.inject.Inject

/**
 * ViewModel class for [BreedHomeFragment] and [BreedHomeScreen]
 */
@HiltViewModel
class BreedHomeViewModel @Inject constructor(
    private val repository: RareBreedRepository
) : ViewModel() {

    val homeBreedState = loadBreedHomeInfo().stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000),
        initialValue = BreedHomeState(isLoading = true)
    )

    fun loadBreedHomeInfo(): Flow<BreedHomeState> {
        return repository.getAllBreeds().map { dogBreedResponseResource ->
            when (dogBreedResponseResource) {
                is Resource.Success -> BreedHomeState(
                    isLoading = false,
                    breeds = dogBreedResponseResource.value.breeds.keys.toList()
                )
                is Resource.Failure -> BreedHomeState(
                    errorMessage = dogBreedResponseResource.errorMessage,
                    isNetworkError = dogBreedResponseResource.isNetworkError
                )
                else -> { BreedHomeState()}
            }
        }
    }

    data class BreedHomeState(
        val isLoading: Boolean = false,
        val breeds: List<String> = listOf(),
        val errorMessage: String? = null,
        val isNetworkError: Boolean = false
    )
}