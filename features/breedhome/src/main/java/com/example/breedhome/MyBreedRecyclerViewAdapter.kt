package com.example.breedhome

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView

import com.example.breedhome.databinding.FragmentBreedHomeBinding

/**
 * [RecyclerView.Adapter] that can display the breeds and navigate when clicked
 */
class MyBreedRecyclerViewAdapter(
    private val values: List<String>,
    private val onBreedClick: (String) -> Unit
) : RecyclerView.Adapter<MyBreedRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(FragmentBreedHomeBinding.inflate(LayoutInflater.from(parent.context), parent, false))

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
       holder.bind(item, onBreedClick)

    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(private val binding: FragmentBreedHomeBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(breed: String, onBreedClick: (String) -> Unit) {
            with(binding) {
                breedName.text = breed
                root.setOnClickListener { onBreedClick(breed) }
            }
        }

    }

}