package com.example.breedhome

import com.example.data.FakeRareBreedRepositoryImpl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class BreedHomeViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private val repository = FakeRareBreedRepositoryImpl()

    private lateinit var viewModel: BreedHomeViewModel

    @Before
    fun setup() {

        viewModel = BreedHomeViewModel(repository)
    }

    @Test
    fun `loadBreedHomeInfo_initial_state`() = runTest {

        val job = backgroundScope.launch(coroutineRule.testDispatcher) {
            viewModel.homeBreedState.collect { state ->
                assert(state.isLoading)
                assert(state.breeds.isEmpty())
                assert(state.errorMessage == null)
                assert(!state.isNetworkError)
            }
        }
        job.cancel()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `loadBreedHomeInfo_success`() = runTest {

        val states = mutableListOf<BreedHomeViewModel.BreedHomeState>()
        val job = backgroundScope.launch(coroutineRule.testDispatcher) {
            viewModel.homeBreedState.collect { state ->
                states.add(state)
            }
        }

        val initialLoadingState = states.first()
        assert(initialLoadingState.isLoading)
        assert(initialLoadingState.breeds.isEmpty())
        assert(initialLoadingState.errorMessage == null)
        assert(!initialLoadingState.isNetworkError)

        advanceUntilIdle()

        val successState = states.last()
        assert(!successState.isLoading)
        assert(successState.breeds.isNotEmpty())
        assert(successState.errorMessage == null)
        assert(!successState.isNetworkError)

        job.cancel()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `loadBreedHomeInfo failure_error_state`() = runTest {

        repository.setSuccess(false)
        val states = mutableListOf<BreedHomeViewModel.BreedHomeState>()
        val job = backgroundScope.launch(coroutineRule.testDispatcher) {
            viewModel.homeBreedState.collect { state ->
                states.add(state)
            }
        }

        val initialLoadingState = states.first()
        assert(initialLoadingState.isLoading)
        assert(initialLoadingState.breeds.isEmpty())
        assert(initialLoadingState.errorMessage == null)
        assert(!initialLoadingState.isNetworkError)

        advanceUntilIdle()

        val errorState = states.last()
        assert(!errorState.isLoading)
        assert(errorState.breeds.isEmpty())
        assert(errorState.errorMessage?.isNotEmpty()  == true)
        assert(errorState.isNetworkError)

        job.cancel()
    }

}