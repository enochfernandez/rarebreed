package com.example.breeddetail

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.MaterialTheme.typography
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import coil.compose.AsyncImage
import com.example.ui.FailureDialog
import com.example.ui.LoaderDialog

@Composable
fun BreedDetailScreen (
    viewModel: BreedDetailViewModel,
    onNavigateUp: () -> Unit
) {

    val state by viewModel.breedDetailState.collectAsStateWithLifecycle()
    BreedDetailContent(
        isLoading = state.isLoading,
        subBreeds = state.subBreeds,
        breed = viewModel.breedId,
        breedsImageUrls = state.breedsUrlStrings,
        error = state.errorMessage,
        isNetworkError = state.isNetworkError,
        onSubBreedSelected = viewModel::getRandomSubBreedsByImage,
        onNavigateUp = onNavigateUp

    )
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun BreedDetailContent(
    isLoading: Boolean,
    subBreeds: List<String>,
    breedsImageUrls: List<String>,
    error: String?,
    isNetworkError: Boolean,
    onNavigateUp: () -> Unit,
    breed: String,
    onSubBreedSelected: (String?) -> Unit
) {
    var rememberDialogDismiss = remember { mutableStateOf(false) }
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Row {
                        val image = painterResource(id = R.drawable.dog)
                        Image(painter = image, contentDescription = "Breed Icon")
                        Spacer(modifier = Modifier.width(12.dp))
                        Text(
                            text = "RareBreed",
                            textAlign = TextAlign.Start,
                            color = MaterialTheme.colorScheme.scrim,
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                },
                navigationIcon = {
                    IconButton(
                        modifier = Modifier.padding(start = 4.dp),
                        onClick = onNavigateUp
                    ) {
                        Icon(
                            painterResource(R.drawable.ic_back),
                            "Back",
                            tint = MaterialTheme.colorScheme.scrim
                        )
                    }
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.surface,
                    titleContentColor = MaterialTheme.colorScheme.onPrimary
                ),

                )
        }
    ) { padding ->
        if (isLoading) {
            LoaderDialog()
        }

        if (error != null || isNetworkError) {
            if (!rememberDialogDismiss.value) {
                FailureDialog(
                    error ?: "Error Occurred, Try Again" ,
                    onDialogDismiss = { rememberDialogDismiss.value = true }
                )
            }
        }

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(padding)
        ) {
            SubBreedTitle()
            Spacer(modifier = Modifier.height(16.dp))
            SubBreedsCategoriesSection(subBreeds, breed, onSubBreedSelected)
            Spacer(modifier = Modifier.height(16.dp))
            SubBreedCategoriesImages(breedsImageUrls)

        }

    }
}

@Composable
fun SubBreedTitle() {
    Text(text = "Sub Breeds", style = typography.headlineLarge, modifier = Modifier.padding(horizontal = 16.dp))
}

@Composable
fun SubBreedsCategoriesSection(
    subBreeds: List<String>,
    breed: String,
    onSubBreedSelected: (String?) -> Unit,
) {
    val subBreedsIncludingParentBreed =
        // Add an element at the first index
        if (subBreeds.isNotEmpty()) listOf(breed) + subBreeds else subBreeds

    LazyRow(
        contentPadding = PaddingValues(horizontal = 16.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp)
    ) {
        itemsIndexed(subBreedsIncludingParentBreed) { index, item ->
            SubBreedTextItem(text = item) {
                if (index == 0) {
                    onSubBreedSelected(null)
                } else {
                    onSubBreedSelected(item)
                }
            }
        }
    }
}

@Composable
fun SubBreedCategoriesImages(breedsImageUrls: List<String>) {
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        contentPadding = PaddingValues(16.dp),
        content = {
            items(breedsImageUrls) {
                SubBreedCardItem(it)
            }
        }
    )
}

@Composable
fun SubBreedTextItem(text: String, onSubBreedSelected: () -> Unit) {
    Box(
        modifier = Modifier
            .padding(4.dp)
            .background(Color.Transparent)
            .border(
                width = 1.dp,
                color = Color.Black, // Border color
                shape = RoundedCornerShape(16.dp), // Rounded corners
            )
            .padding(8.dp) // Adjust padding as needed
    ) {
        Text(
            text = text,
            modifier = Modifier
                .padding(8.dp)
                .clickable { onSubBreedSelected() },
            style = typography.bodyLarge,
        )
    }
}

@Composable
fun SubBreedCardItem(urlString: String) {
    Card (
        modifier = Modifier
            .padding(8.dp)
            .size(200.dp),
        elevation = CardDefaults.cardElevation(8.dp),
    ) {
        AsyncImage(
            model = urlString,
            contentDescription = "Sub Breed Image",
            contentScale = ContentScale.Crop,
            modifier = Modifier.fillMaxSize()
        )
    }
}

@Preview(showBackground = true, backgroundColor = 0xFFF0EAE2)
@Composable
fun PreviewAreaViewPager(modifier: Modifier = Modifier) {
    BreedDetailContent(
        isLoading = false,
        subBreeds = listOf("hound", "afgan", "bulldog"),
        breedsImageUrls = listOf(
            "https://images.dog.ceo/breeds/terrier-silky/n02097658_1270.jpg",
            "https://images.dog.ceo/breeds/keeshond/n02112350_8310.jpg",
            "https://images.dog.ceo/breeds/dachshund/Miniature_Daschund.jpg"
        ),
        error = null,
        isNetworkError = false,
        onNavigateUp = { },
        breed = "hound",
        onSubBreedSelected = { }
    )
}
