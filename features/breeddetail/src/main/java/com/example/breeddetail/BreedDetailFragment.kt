package com.example.breeddetail

import android.os.Bundle
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.setPadding
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.breeddetail.databinding.FragmentBreedDetailBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * A simple Fragment class for displaying data for the details page
 */
@AndroidEntryPoint
class BreedDetailFragment : Fragment() {

    private var _binding: FragmentBreedDetailBinding? = null
    private val binding get() = _binding!!
    private var rememberBreedIdAdded = false

    @Inject
    lateinit var viewModelAssistedFactory: BreedDetailViewModel.Factory

    val viewModel: BreedDetailViewModel by viewModels {
        arguments?.getString("param1")?.let { breed ->
            BreedDetailViewModel.provideFactory(viewModelAssistedFactory, breed)
        } ?: throw IllegalStateException("'noteId' shouldn't be null")
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentBreedDetailBinding.inflate(inflater, container, false)
        var view = binding.root
        binding.breedDetailRecyclerView.layoutManager = GridLayoutManager(requireContext(), 2)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.breedDetailState.collect {
                    binding.breedDetailRecyclerView.adapter = BreedDetailImageGridAdapter(it.breedsUrlStrings)

                    populateSubBreeds(it.subBreeds)
                }
            }
        }
    }


    private fun populateSubBreeds(subBreeds: List<String>) {
        val subBreedsIncludingParentBreed = if (subBreeds.isNotEmpty()) {
            arguments?.getString("param1")?.let {
                listOf(it) + subBreeds
            }
        } else subBreeds


        subBreedsIncludingParentBreed?.forEachIndexed { index, value ->
            val textView = TextView(context).apply {
                layoutParams =
                    LinearLayout.LayoutParams(
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT,
                    ).apply {
                        marginStart = resources.getDimensionPixelSize(R.dimen.text_view_margin)
                        marginEnd = resources.getDimensionPixelSize(R.dimen.text_view_margin)
                    }
                gravity = Gravity.CENTER
                setBackgroundResource(R.drawable.sub_breed_shape)
                setPadding(resources.getDimensionPixelSize(R.dimen.text_view_padding_start))
                text = value
            }
            textView.setOnClickListener {
                if (index == 0) {
                    viewModel.getRandomSubBreedsByImage(null)
                } else {
                    arguments?.getString("param1")?.let {
                        viewModel.getRandomSubBreedsByImage(it)
                    }
                }
            }
            binding.subBreedListLinearLayout.addView(textView)
        }
    }

}