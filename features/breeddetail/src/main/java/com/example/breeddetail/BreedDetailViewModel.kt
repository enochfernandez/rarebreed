package com.example.breeddetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.connectivity.Resource
import com.example.data.RareBreedRepository
import dagger.Module
import dagger.assisted.Assisted
import dagger.assisted.AssistedFactory
import dagger.assisted.AssistedInject
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.stateIn


/**
 * ViewModel class associated with [BreedDetailScreen] and [BreedDetailFragment]
 */
class BreedDetailViewModel @AssistedInject constructor(
    private val repository: RareBreedRepository,
    @Assisted val breedId: String
) : ViewModel() {

    private val breedType = MutableStateFlow<String?>(null)

    val breedDetailState = loadBreedSubSectionAndImages(breedId, IMAGE_QUANTITY).stateIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(5_000),
        initialValue = BreedDetailState(isLoading = true)
    )

    fun loadBreedSubSectionAndImages(breed: String, imageQuantity: Int): Flow<BreedDetailState> {
        val subBreedsFlow = repository.getSubBreeds(breed)
        val randomBreedImagesFlow = breedType.flatMapLatest { breedType ->
            breedType?.let {
                repository.getRandomSubBreedsByImage(breed, it, imageQuantity)
            } ?: repository.getRandomBreedsByImage(breed, imageQuantity)
        }

        return combine(
            subBreedsFlow,
            randomBreedImagesFlow
        ) { subBreedsResponseResource, dogBreedRandomImageResource ->
            when {
                    subBreedsResponseResource is Resource.Success && dogBreedRandomImageResource is Resource.Success -> {
                    BreedDetailState(
                        isLoading = false,
                        subBreeds = subBreedsResponseResource.value.subBreeds,
                        breedsUrlStrings = dogBreedRandomImageResource.value.breedImageUrls,
                        errorMessage = null,
                        isNetworkError = false
                    )
                }
                subBreedsResponseResource is Resource.Failure -> {
                    BreedDetailState(
                        isLoading = false,
                        errorMessage = subBreedsResponseResource.errorMessage,
                        isNetworkError = subBreedsResponseResource.isNetworkError
                    )
                }
                dogBreedRandomImageResource is Resource.Failure -> {
                    BreedDetailState(
                        isLoading = false,
                        errorMessage = dogBreedRandomImageResource.errorMessage,
                        isNetworkError = dogBreedRandomImageResource.isNetworkError
                    )
                }
                else -> {  BreedDetailState()}
            }
        }
    }

    fun getRandomSubBreedsByImage(breed: String?) {
        breedType.value = breed
    }

    data class BreedDetailState(
        val isLoading: Boolean = false,
        val subBreeds: List<String> = listOf(),
        val breedsUrlStrings: List<String> = listOf(),
        val errorMessage: String? = null,
        val isNetworkError: Boolean = false
    )



    @AssistedFactory
    interface Factory {
        fun create(breedId: String) : BreedDetailViewModel
    }

    @Suppress("UNCHECKED_CAST")
    companion object {
        const val IMAGE_QUANTITY = 10
        fun provideFactory(
            assistedFactory: Factory,
            breedId: String
        ): ViewModelProvider.Factory = object : ViewModelProvider.Factory {
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return assistedFactory.create(breedId) as T
            }
        }
    }
}

@Module
@InstallIn(ActivityRetainedComponent::class)
interface AssistedInjectModule