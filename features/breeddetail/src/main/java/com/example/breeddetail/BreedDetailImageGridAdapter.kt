package com.example.breeddetail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import coil.load


/**
 * Adapter to show the data for [BreedDetailFragment]
 */
class BreedDetailImageGridAdapter(private val imageUrls: List<String>)
    : RecyclerView.Adapter<BreedDetailImageGridAdapter.ImageViewHolder>() {

    inner class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageView: ImageView = itemView.findViewById(R.id.imageView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.grid_item_layout, parent, false)
        return ImageViewHolder(view)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val imageUrl = imageUrls[position]

        // Load image into ImageView using Coil
        holder.imageView.load(imageUrl) {
            crossfade(true)
            placeholder(R.drawable.placeholder_2) // Add a placeholder drawable if needed
        }
    }

    override fun getItemCount(): Int {
        return imageUrls.size
    }
}