package com.example.breeddetail

import com.example.data.FakeRareBreedRepositoryImpl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.MockitoAnnotations

class BreedDetailViewModelTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @get:Rule
    val coroutineRule = MainCoroutineRule()

    private var repository = FakeRareBreedRepositoryImpl()

    private lateinit var viewModel: BreedDetailViewModel

    private val breedID = "hound"

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        viewModel = BreedDetailViewModel(repository, breedID)
    }

    @Test
    fun `loadBreedSubSectionAndImages_initial_state`() = runTest {

        val job = backgroundScope.launch(coroutineRule.testDispatcher) {
            viewModel.breedDetailState.collect { initialState ->
                assert(initialState.isLoading)
                assert(initialState.subBreeds.isEmpty())
                assert(initialState.breedsUrlStrings.isEmpty())
                assert(!initialState.isNetworkError)
                assert(initialState.errorMessage == null)
            }
        }
        job.cancel()
    }

    @Test
    fun `loadAllBreedSubSectionAndImages_success_state`() = runTest {

        val states = mutableListOf<BreedDetailViewModel.BreedDetailState>()
        val job = backgroundScope.launch(coroutineRule.testDispatcher) {
            viewModel.breedDetailState.collect { state ->
                states.add(state)
            }
        }
        val initialState = states.first()
        assert(initialState.isLoading)
        assert(initialState.subBreeds.isEmpty())
        assert(initialState.breedsUrlStrings.isEmpty())
        assert(!initialState.isNetworkError)
        assert(initialState.errorMessage == null)

        advanceUntilIdle()

        val successState = states.last()
        assert(!successState.isLoading)
        assert(successState.subBreeds == repository.sampleDogSubBreedsResponse.subBreeds)
        assert(successState.breedsUrlStrings == repository.sampleDogBreedRandomImagesResponse.breedImageUrls)
        assert(!successState.isNetworkError)
        assert(successState.errorMessage == null)

        job.cancel()

    }

    @Test
    fun `loadBreedSubSectionAndImages_failure_state_for_getSubBreeds`() = runTest {

        repository.setSuccess(false)
        val states = mutableListOf<BreedDetailViewModel.BreedDetailState>()
        val job = backgroundScope.launch(coroutineRule.testDispatcher) {
            viewModel.breedDetailState.collect { state ->
                states.add(state)
            }
        }
        val initialState = states.first()
        assert(initialState.isLoading)
        assert(initialState.subBreeds.isEmpty())
        assert(initialState.breedsUrlStrings.isEmpty())
        assert(!initialState.isNetworkError)
        assert(initialState.errorMessage == null)

        advanceUntilIdle()

        val errorState = states.last()
        assert(!errorState.isLoading)
        assert(errorState.subBreeds.isEmpty())
        assert(errorState.breedsUrlStrings.isEmpty())
        assert(errorState.errorMessage?.isNotEmpty()  == true)
        assert(errorState.isNetworkError)


        job.cancel()
    }
}