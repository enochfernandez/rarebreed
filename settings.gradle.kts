pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}

rootProject.name = "RareBreed"
include(":composeapp")
include(":network")
include(":data")
include(":core:connectivity")
include(":core:ui")
include(":core:model")
include(":features:breeddetail")
include(":features:breedhome")
include(":simpleapp")
