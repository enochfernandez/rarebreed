package com.example.data

import com.example.connectivity.Resource
import com.example.model.DogBreedByImageResponse
import com.example.model.DogBreedRandomImagesResponse
import com.example.model.DogBreedsResponse
import com.example.model.DogSubBreedsResponse
import com.example.network.RareBreedService
import com.example.network.SafeApiCall
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

/**
 * Repository Impl of [RareBreedRepository]
 */
class RareBreedRepositoryImpl @Inject constructor(
    private val breedService: RareBreedService
) : RareBreedRepository, SafeApiCall {

    override fun getAllBreeds(): Flow<Resource<DogBreedsResponse>> {
        return safeApiCall {
            breedService.getAllBreeds()
        }
    }

    override fun getSubBreeds(breed: String): Flow<Resource<DogSubBreedsResponse>> {
        return safeApiCall {
            breedService.getSubBreeds(breed)
        }
    }

    override fun getBreedImage(breed: String): Flow<Resource<DogBreedByImageResponse>> {
        return safeApiCall {
            breedService.getBreedImage(breed)
        }
    }

    override fun getRandomBreedsByImage(
        breed: String,
        images: Int
    ): Flow<Resource<DogBreedRandomImagesResponse>> {
        return safeApiCall {
            breedService.getRandomBreedsByImage(breed, images)
        }
    }

    override fun getRandomSubBreedsByImage(
        breed: String,
        subBreed: String,
        images: Int
    ): Flow<Resource<DogBreedRandomImagesResponse>> {
        return safeApiCall {
            breedService.getRandomSubBreedsByImage(breed, subBreed, images)
        }
    }

}