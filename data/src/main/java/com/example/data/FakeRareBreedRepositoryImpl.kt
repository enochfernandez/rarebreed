package com.example.data

import com.example.connectivity.Resource
import com.example.model.DogBreedByImageResponse
import com.example.model.DogBreedRandomImagesResponse
import com.example.model.DogBreedsResponse
import com.example.model.DogSubBreedsResponse
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
class FakeRareBreedRepositoryImpl : RareBreedRepository  {

    private var emitSuccess = true

    fun setSuccess(success: Boolean) {
        emitSuccess = success
    }
    override fun getAllBreeds(): Flow<Resource<DogBreedsResponse>> {
        return flow {
            delay(2000)
            if (emitSuccess) {
                emit(Resource.Success(sampleDogBreedsResponse))
            } else {
                emit(
                    Resource.Failure(
                        isNetworkError = true,
                        errorCode = 500,
                        errorBody = null,
                        errorMessage = "Failed to fetch data"
                    )
                )
            }
        }
    }

    override fun getSubBreeds(breed: String): Flow<Resource<DogSubBreedsResponse>> {
        return flow {
            delay(2000)
            if (emitSuccess) {
                emit(Resource.Success(sampleDogSubBreedsResponse))
            } else {
                emit(
                    Resource.Failure(
                        isNetworkError = true,
                        errorCode = 500,
                        errorBody = null,
                        errorMessage = "Failed to fetch data"
                    )
                )
            }
        }
    }

    override fun getBreedImage(breed: String): Flow<Resource<DogBreedByImageResponse>> {
        return flow {
            delay(2000)
            if (emitSuccess) {
                emit(Resource.Success(sampleDogBreedByImageResponse))
            } else {
                emit(
                    Resource.Failure(
                        isNetworkError = true,
                        errorCode = 500,
                        errorBody = null,
                        errorMessage = "Failed to fetch data"
                    )
                )
            }
        }
    }

    override fun getRandomBreedsByImage(
        breed: String,
        images: Int
    ): Flow<Resource<DogBreedRandomImagesResponse>> {
        return flow {
            delay(2000)
            if (emitSuccess) {
                emit(Resource.Success(sampleDogBreedRandomImagesResponse))
            } else {
                emit(
                    Resource.Failure(
                        isNetworkError = true,
                        errorCode = 500,
                        errorBody = null,
                        errorMessage = "Failed to fetch data"
                    )
                )
            }
        }
    }

    override fun getRandomSubBreedsByImage(
        breed: String,
        subBreed: String,
        images: Int
    ): Flow<Resource<DogBreedRandomImagesResponse>> {
        return flow {
            delay(2000)
            if (emitSuccess) {
                emit(Resource.Success(sampleDogBreedRandomImagesResponse))
            } else {
                emit(
                    Resource.Failure(
                        isNetworkError = true,
                        errorCode = 500,
                        errorBody = null,
                        errorMessage = "Failed to fetch data"
                    )
                )
            }
        }
    }

    val sampleDogBreedsResponse = DogBreedsResponse(
        breeds = mapOf(
            "bulldog" to listOf("boston", "english"),
            "hound" to listOf("afghan", "afghan"),
            "greyhound" to listOf("italian")
        ),
        status = "success"
    )

    val sampleDogSubBreedsResponse = DogSubBreedsResponse(
        subBreeds = listOf("boston", "english"),
        status = "success"
    )

    val sampleDogBreedRandomImagesResponse = DogBreedRandomImagesResponse(
        breedImageUrls = listOf("url1", "url2"),
        status = "success"
    )

    val sampleDogBreedByImageResponse = DogBreedByImageResponse(
        imageUrl = "https://example.com/image1.jpg",
        status = "success"
    )
}