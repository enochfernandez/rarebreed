package com.example.data

import com.example.connectivity.Resource
import com.example.model.DogBreedByImageResponse
import com.example.model.DogBreedRandomImagesResponse
import com.example.model.DogBreedsResponse
import com.example.model.DogSubBreedsResponse
import kotlinx.coroutines.flow.Flow

/**
 * Repository representation of the data from the RareBreedService
 */
interface RareBreedRepository {

    /**
     * Get all Dog breeds
     */
    fun getAllBreeds(): Flow<Resource<DogBreedsResponse>>

    /**
     * Get subBreeds by breed
     * @param breed specific breed whose sub breeds are is required
     */
    fun getSubBreeds(breed: String): Flow<Resource<DogSubBreedsResponse>>

    /**
     * Get image of breed by breed
     * @param breed specific breed whose image is asked for.
     */
    fun getBreedImage(breed: String): Flow<Resource<DogBreedByImageResponse>>

    /**
     * Get random images of breeds
     * @param breed specific breed whose images are required
     * @param images the quantity of images needed
     */
    fun getRandomBreedsByImage(breed: String,images: Int): Flow<Resource<DogBreedRandomImagesResponse>>

    /**
     * Get random images of sub breeds
     * @param breed specific breed whose images are required
     * @param subBreed specific sub breed whose images are required
     * @param images the quantity of images needed
     */
    fun getRandomSubBreedsByImage(
        breed: String,
        subBreed: String,
        images: Int
    ): Flow<Resource<DogBreedRandomImagesResponse>>
}