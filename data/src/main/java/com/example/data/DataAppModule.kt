package com.example.data

import com.example.connectivity.NetworkMonitor
import com.example.data.util.ConnectivityNetworkMonitor
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Dagger Hilt Module for Data classes.
 */
@Module
@InstallIn(SingletonComponent::class)
interface DataAppModule {

    @Binds
    fun bindRareBreedRepository(rareBreedRepositoryImpl: RareBreedRepositoryImpl): RareBreedRepository

    @Binds
    fun bindsNetworkMonitor(
        networkMonitor: ConnectivityNetworkMonitor,
    ): NetworkMonitor
}