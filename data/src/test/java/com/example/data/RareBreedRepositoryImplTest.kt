package com.example.data

import com.example.connectivity.Resource
import com.example.model.DogBreedByImageResponse
import com.example.model.DogBreedRandomImagesResponse
import com.example.model.DogBreedsResponse
import com.example.model.DogSubBreedsResponse
import com.example.network.RareBreedService
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class RareBreedRepositoryImplTest {

    @Mock
    private lateinit var breedService: RareBreedService

    private lateinit var repository: RareBreedRepositoryImpl

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repository = RareBreedRepositoryImpl(breedService)
    }

    @Test
    fun `test getAllBreeds`() = runBlocking {
        // Sample data
        val sampleData = DogBreedsResponse(
            breeds = mapOf(
                "breed1" to emptyList(),
                "breed2" to listOf("sub1", "sub2")
            ),
            status = "success"
        )

        // Mock the service response
        `when`(breedService.getAllBreeds()).thenReturn(sampleData)

        // Call the repository method
        val result = repository.getAllBreeds().first()

        // Verify the result
        assertEquals(Resource.Success(sampleData), result)
    }

    @Test
    fun `test getSubBreeds`() = runBlocking {
        // Sample data
        val breed = "breed1"
        val sampleData = DogSubBreedsResponse(
            subBreeds = listOf("sub1", "sub2"),
            status = "success"
        )

        // Mock the service response
        `when`(breedService.getSubBreeds(breed)).thenReturn(sampleData)

        // Call the repository method
        val result = repository.getSubBreeds(breed).first()

        // Verify the result
        assertEquals(Resource.Success(sampleData), result)
    }


    @Test
    fun `test getBreedImage`() = runBlocking {
        // Sample data
        val breed = "breed1"
        val sampleData = DogBreedByImageResponse(
            imageUrl = "https://example.com/image1.jpg",
            status = "success"
        )

        // Mock the service response
        Mockito.`when`(breedService.getBreedImage(breed)).thenReturn(sampleData)

        // Call the repository method
        val result = repository.getBreedImage(breed).first()

        // Verify the result
        assertEquals(Resource.Success(sampleData), result)
    }

    @Test
    fun `test getRandomBreedsByImage`() = runBlocking {
        // Sample data
        val breed = "breed1"
        val images = 5
        val sampleData = DogBreedRandomImagesResponse(
            breedImageUrls = listOf("https://example.com/image1.jpg", "https://example.com/image2.jpg"),
            status = "success"
        )

        // Mock the service response
        Mockito.`when`(breedService.getRandomBreedsByImage(breed, images)).thenReturn(sampleData)

        // Call the repository method
        val result = repository.getRandomBreedsByImage(breed, images).first()

        // Verify the result
        assertEquals(Resource.Success(sampleData), result)
    }

    @Test
    fun `test getRandomSubBreedsByImage`() = runBlocking {
        // Sample data
        val breed = "breed1"
        val subBreed = "sub1"
        val images = 3
        val sampleData = DogBreedRandomImagesResponse(
            breedImageUrls = listOf("https://example.com/image1.jpg", "https://example.com/image2.jpg"),
            status = "success"
        )

        // Mock the service response
        Mockito.`when`(breedService.getRandomSubBreedsByImage(breed, subBreed, images)).thenReturn(sampleData)

        // Call the repository method
        val result = repository.getRandomSubBreedsByImage(breed, subBreed, images).first()

        // Verify the result
        assertEquals(Resource.Success(sampleData), result)
    }
}