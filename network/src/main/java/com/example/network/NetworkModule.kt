package com.example.network

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Dagger Hilt Module for Network feature transactions.
 * This provides dependencies required at the Application level.
 */

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    private val baseRetrofitBuilder: Retrofit.Builder = Retrofit.Builder()
        .baseUrl(Constants.API_BASE_URL)
        .addConverterFactory(MoshiConverterFactory.create())

    private var logging =  HttpLoggingInterceptor()

    private val okHttpClientBuilder: OkHttpClient.Builder = OkHttpClient.Builder()

    @Provides
    fun provideBreedService(): RareBreedService {
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        return baseRetrofitBuilder
            .client(okHttpClientBuilder
                .addInterceptor(logging).build()
            )
            .build()
            .create(RareBreedService::class.java)
    }
}