/**
 * General constant values to be used across the app
 */
object Constants {
    const val API_BASE_URL = "https://dog.ceo/api/"
}