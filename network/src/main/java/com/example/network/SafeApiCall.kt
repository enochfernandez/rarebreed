package com.example.network

import com.example.connectivity.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.retryWhen
import retrofit2.HttpException
import java.io.IOException

/**
 * Safely Call for Api calls that returns a success or failure
 */
interface SafeApiCall {
    fun <T> safeApiCall(
        apiCall: suspend () -> T
    ) : Flow<Resource<T>> {
        return flow <Resource<T>> {
            emit(Resource.Success(apiCall.invoke()))
        }
            .flowOn(Dispatchers.IO)
            .retryWhen { cause, attempt ->
                if (cause is IOException && attempt < 3) {
                    delay(2000)
                    return@retryWhen true
                } else {
                    return@retryWhen false
                }
            }.catch { cause ->
                when (cause) {
                    is HttpException -> {
                        emit(Resource.Failure(
                            isNetworkError = false,
                            cause.code(),
                            cause.response()?.errorBody(),
                            cause.response()?.message() ?: ""
                        ))
                    }
                    else-> {
                        emit(Resource.Failure(
                            true,
                            null,
                            null,
                            cause.message ?: ""
                        ))
                    }
                }
            }
    }

}