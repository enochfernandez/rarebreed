package com.example.network

import com.example.model.DogBreedByImageResponse
import com.example.model.DogBreedRandomImagesResponse
import com.example.model.DogBreedsResponse
import com.example.model.DogSubBreedsResponse
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Retrofit Api declaration for retrieving data
 */
interface RareBreedService {

    @GET("breeds/list/all")
    suspend fun getAllBreeds(): DogBreedsResponse

    @GET("breed/{breed}/list")
    suspend fun getSubBreeds(@Path("breed") breed: String): DogSubBreedsResponse

    @GET("breed/{breed}/images/random")
    suspend fun getBreedImage(@Path("breed") breed: String): DogBreedByImageResponse

    @GET("breed/{breed}/images/random/{images}")
    suspend fun getRandomBreedsByImage(@Path("breed") breed: String, @Path("images") images: Int): DogBreedRandomImagesResponse

    @GET("breed/{breed}/{subBreed}/images/random/{images}")
    suspend fun getRandomSubBreedsByImage(@Path("breed") breed: String, @Path("subBreed") subBreed: String, @Path("images") images: Int): DogBreedRandomImagesResponse
}