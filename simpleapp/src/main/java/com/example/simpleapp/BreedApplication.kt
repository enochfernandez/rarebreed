package com.example.simpleapp

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Application class for BreedComposeApp
 */
@HiltAndroidApp
class BreedApplication : Application() {

}